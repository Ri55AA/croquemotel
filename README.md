# CroqueMotel

Re-development of Motion Twin's CroqueMotel game ([hotel.muxxu.com](http://hotel.muxxu.com))

All graphic assets are licensed under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/), the rest of the project is licensed under [AGPL-3.0-or-later](http://www.gnu.org/licenses/agpl-3.0.html).

## Tech stack

- **Frontend:** *TypeScript*, *Sass*, NuxtJS (Vue.js), PIXI
- **Backend:** *TypeScript*, Node.js, Fastify, TypeORM

## Setup

1. Install *Node.js* (>= v14): https://nodejs.org/en/download/
2. Install *Yarn*: 
    ```
    npm install -g yarn
    ```
3. Clone (or fork) this repository
4. Copy `.env.exemple` file to `.env`
5. Go to `app/` folder
6. Install dependencies: 
    ```
    yarn
    ```
7. Go to `etwin/` folder
8. Copy `etwin.toml.example` to `etwin.toml`

## Launching the app

### Using Docker

Inside the root folder run:
```
docker-compose up --build
```

You might need to install dependencies inside `app/` and `etwin/` before launching (by running `yarn` command).

### Legacy method

#### Database

Install and start PostgreSQL (following [this guide](https://gitlab.com/eternal-twin/etwin/-/blob/master/docs/db.md))

#### ETwin

1. Inside `etwin/` folder (the first time, to install dependencies), run:
    ```
    yarn
    ``` 
2. Start ETwin website:
    ```
    yarn start
    ```

#### App

Inside `app/` folder, run:
- for simple launch:
    ```
    yarn build && yarn start
    ```
- Or for live-reload:
    ```
    yarn dev
    ```

### Using ETwin with the Postgres database

If you want ot use ETwin with your Postgres database for data persistence, you need to initialize it.

1. Clone [the main ETwin repository](https://gitlab.com/eternal-twin/etwin) in a dedicated folder
1. Run `git checkout 73b6091` to use ETwin 0.4.0
1. Follow the [*Getting started*](https://gitlab.com/eternal-twin/etwin#getting-started) instructions 
    > Make sure your `etwin.toml` reference the same database
1. Run `yarn run db:create`
