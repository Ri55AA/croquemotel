import { Application, Sprite } from "pixi.js";

export class Game {
  private _app: Application;

  constructor(view: HTMLCanvasElement, width: number, height: number, imgPath: string) {
    this._app = new Application({
      view,
      width,
      height
    });

    this._app.loader.add("blob", imgPath).load((_loader, resources) => {
      const blob = new Sprite(resources.blob.texture);

      blob.x = this._app.renderer.width / 2;
      blob.y = this._app.renderer.height / 2;

      blob.anchor.x = 0.5;
      blob.anchor.y = 0.5;

      this._app.stage.addChild(blob);

      this._app.ticker.add(() => {
        // each frame we spin the bunny around a bit
        blob.rotation += 0.01;
      });
    });
  }
}
