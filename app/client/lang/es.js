export default {
  welcome: "Bienvenido",
  menu: {
    index: "Descubre el juego",
    play: "¡Juega ahora!",
    visit: "Visita un hotel al azar",
    help: "Guía",
    office: null,
    lab: null,
    shop: null,
    friends: null
  }
};
