export default {
  welcome: "Willkommen",
  menu: {
    index: "Entdecke das Spiel",
    play: "Spielen",
    visit: "Besuche ein beliebiges Hotel",
    help: "Spielhilfe",
    office: null,
    lab: null,
    shop: null,
    friends: null
  }
};
