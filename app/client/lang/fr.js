export default {
  welcome: "Bienvenue",
  menu: {
    index: "Découvrir le jeu",
    play: "Jouer",
    visit: "Voir un hôtel au hasard",
    help: "Aide",
    office: "Bureau",
    lab: "Labo",
    shop: "Supermarché",
    friends: "Mes amis"
  }
};
