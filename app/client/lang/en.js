export default {
  welcome: "Welcome",
  menu: {
    index: "Discover the game",
    play: "Play",
    visit: "Visit a random hotel",
    help: "Help",
    office: "Office",
    lab: "Lab",
    shop: "Supermarket",
    friends: "My friends"
  }
};
