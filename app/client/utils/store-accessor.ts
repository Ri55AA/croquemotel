import { Store } from "vuex";
import { getModule } from "vuex-module-decorators";

import AuthStore from "~/store/auth";

let authStore: AuthStore;

function initialiseStores(store: Store<any>): void {
  authStore = getModule(AuthStore, store);
}

export { authStore, initialiseStores };
