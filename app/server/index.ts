import "reflect-metadata";

import { listen, setup } from "./main.js";

setup().then(listen);
