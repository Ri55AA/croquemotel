import { FastifyReply, FastifyRequest } from "fastify";
import { Controller, GET, POST } from "fastify-decorators";

import { authenticate } from "../auth/auth.helper.js";

@Controller("/ping")
export default class PingHandler {
  @GET()
  public get(request: FastifyRequest, reply: FastifyReply) {
    reply.send(request.session.get("ping") ?? {});
  }

  @POST("", {
    preHandler: authenticate
  })
  public post(request: FastifyRequest, reply: FastifyReply) {
    const body = request.body ?? {};
    request.session.set("ping", body);
    reply.send(body);
  }
}
