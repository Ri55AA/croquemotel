import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";

import { Hotel } from "../../hotel/entities/hotel.entity.js";
import { GuestLikes } from "../enums/guest-likes.enum.js";
import { GuestType, GuestTypeTransformer } from "../enums/guest-type.enum.js";

@Entity()
export class Guest {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column()
  hotelId!: string;

  @ManyToOne("Hotel")
  @JoinColumn({ name: "hotelId" })
  hotel!: Hotel;

  @Column({ type: "varchar", transformer: new GuestTypeTransformer() })
  type?: GuestType;

  @Column()
  name?: string;

  @Column({ enum: GuestLikes })
  likes?: GuestLikes;

  @Column({ enum: GuestLikes })
  dislikes?: GuestLikes;

  @Column({ enum: GuestLikes })
  effects?: GuestLikes;

  @Column({ default: false })
  isVip!: boolean;

  @CreateDateColumn()
  createdAt?: Date;

  @UpdateDateColumn()
  updatedAt?: Date;
}
