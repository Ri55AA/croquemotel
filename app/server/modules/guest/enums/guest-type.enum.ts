import { ValueTransformer } from "typeorm";

export class GuestType {
  static AQUA = new GuestType("aqua");
  static FIRE = new GuestType("fire");
  static BLOB = new GuestType("blob");
  static GHOST = new GuestType("ghost");
  static DOLL = new GuestType("doll");
  static BOMB = new GuestType("bomb");
  static VEGETAL = new GuestType("vegetal");
  static BUSINESS = new GuestType("business");
  static FRANK = new GuestType("frank");
  static GIFT = new GuestType("gift");
  static WITCH = new GuestType("witch");
  static ZOMBIE = new GuestType("zombie");
  static FLYING = new GuestType("flying");

  private constructor(
    public name: string
  ) { }

  static from(name: string): GuestType {
    return Object.values(GuestType).find(value => value instanceof GuestType && value.name === name);
  }
}

export class GuestTypeTransformer implements ValueTransformer {
  to(value: GuestType): any {
    return value?.name;
  }

  from(value: any): GuestType {
    return GuestType.from(value);
  }
}
