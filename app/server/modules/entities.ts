// PATCH: https://stackoverflow.com/questions/66735359/error-when-using-typeorm-and-typescript-with-folder-path
export * from "./guest/entities/guest.entity.js";
export * from "./hotel/entities/floor.entity.js";
export * from "./hotel/entities/hotel.entity.js";
export * from "./hotel/entities/room.entity.js";
export * from "./hotel/entities/staff.entity.js";
