import { FastifyReply, FastifyRequest } from "fastify";
import { Controller, GET } from "fastify-decorators";

import { authenticate } from "../../auth/auth.helper.js";
import { HotelService } from "../hotel.service.js";
import { HotelFieldsSchema, HotelFieldsType } from "./schemas/hotel-fields.schema.js";

@Controller("/api/hotel")
export default class HotelHandler {
  constructor(
    private hotelService: HotelService
  ) { }

  @GET("/:id", {
    schema: {
      params: {
        id: { type: "number" }
      },
      querystring: HotelFieldsSchema
    }
  })
  public async get(request: FastifyRequest<{ Params: { id: number }, Querystring: HotelFieldsType }>, reply: FastifyReply) {
    const result = await this.hotelService.findHotel(request.params.id, request.query.fields);
    reply.sendServiceResult(result);
  }

  @GET("", {
    preHandler: authenticate,
    schema: {
      querystring: HotelFieldsSchema
    }
  })
  public async getCurrent(request: FastifyRequest<{ Querystring: HotelFieldsType }>, reply: FastifyReply) {
    const hotelId = request.getHotelId()!;
    const result = await this.hotelService.findHotel(hotelId, request.query.fields);
    reply.sendServiceResult(result, { groups: ["player"] });
  }
}
