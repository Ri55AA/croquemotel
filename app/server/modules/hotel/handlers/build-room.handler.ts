import { Static, Type } from "@sinclair/typebox";
import { FastifyReply, FastifyRequest } from "fastify";
import { Controller, PATCH, POST } from "fastify-decorators";

import { ServiceResult } from "../../../services/service.js";
import { authenticate } from "../../auth/auth.helper.js";
import { BuildService } from "../build.service.js";
import { Room } from "../entities/room.entity.js";

const PostBuildRoom = Type.Object({
  room: Type.Number({ minimum: 1 }),
  type: Type.Union([
    Type.String({ pattern: "bed" }),
    Type.String({ pattern: "service" }),
    Type.String({ pattern: "special" })
  ]),
  service: Type.Optional(Type.String())
});
type PostBuildRoom = Static<typeof PostBuildRoom>;

const PatchBuildRoom = Type.Object({
  room: Type.Number({ minimum: 1 }),
  type: Type.String({ pattern: "bed" })
});
type PatchBuildRoom = Static<typeof PatchBuildRoom>;

@Controller("/api/hotel/build/room")
export default class BuildRoomHandler {
  constructor(
    private buildService: BuildService
  ) { }

  @POST("", {
    preHandler: authenticate,
    schema: {
      body: PostBuildRoom
    }
  })
  public async post(request: FastifyRequest<{ Body: PostBuildRoom }>, reply: FastifyReply) {
    const hotelId = request.getHotelId()!;

    let result: ServiceResult<Room>;

    switch (request.body.type) {
      case "bed":
        result = await this.buildService.buildNewBedRoom(hotelId, request.body.room);
        break;
      case "service":
        result = await this.buildService.buildNewServiceRoom(hotelId, request.body.room, request.body.service!);
        break;
      case "special":
        result = await this.buildService.buildNewSpecialRoom(hotelId, request.body.room, request.body.service!);
        break;
      default:
        return;
    }

    reply.sendServiceResult(result, { groups: ["player"], successCode: 201 });
  }

  @PATCH("", {
    preHandler: authenticate,
    schema: {
      body: PatchBuildRoom
    }
  })
  public async patch(request: FastifyRequest<{ Body: PatchBuildRoom }>, reply: FastifyReply) {
    const hotelId = request.getHotelId()!;

    let result: ServiceResult<Room>;

    switch (request.body.type) {
      case "bed":
        result = await this.buildService.upgradeBedRoom(hotelId, request.body.room);
        break;
      default:
        return;
    }

    reply.sendServiceResult(result, { groups: ["player"], successCode: 200 });
  }
}
