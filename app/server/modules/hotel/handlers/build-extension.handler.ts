import { Static, Type } from "@sinclair/typebox";
import { FastifyReply, FastifyRequest } from "fastify";
import { Controller, POST } from "fastify-decorators";

import { authenticate } from "../../auth/auth.helper.js";
import { BuildService } from "../build.service.js";

const BuildExtension = Type.Object({
  floorLevel: Type.Number({ minimum: 0 }),
  position: Type.Number({ minimum: -1 })
});
type BuildExtension = Static<typeof BuildExtension>;

@Controller("/api/hotel/build/extension")
export default class BuildExtensionHandler {
  constructor(
    private buildService: BuildService
  ) { }

  @POST("", {
    preHandler: authenticate,
    schema: {
      body: BuildExtension
    }
  })
  public async post(request: FastifyRequest<{ Body: BuildExtension }>, reply: FastifyReply) {
    const hotelId = request.getHotelId()!;
    const result = await this.buildService.buildNewExtension(hotelId, request.body);
    reply.sendServiceResult(result, { groups: ["player"], successCode: 201 });
  }
}
