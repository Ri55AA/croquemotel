import { Static, Type } from "@sinclair/typebox";

export const HotelFieldSchema = Type.String();

export const HotelFieldsSchema = Type.Object({
  fields: Type.Optional(Type.Array(HotelFieldSchema))
});

export type HotelFieldsType = Static<typeof HotelFieldsSchema>;
