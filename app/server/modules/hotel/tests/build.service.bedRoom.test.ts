import anyTest, { TestInterface } from "ava";
import { configureServiceTest } from "fastify-decorators/testing";
import { EntityTarget } from "typeorm";

import { ServiceResultStatus } from "../../../services/service.js";
import { DatabaseServiceMock } from "../../../tests/database.service.mock.js";
import { BuildService } from "../build.service.js";
import { Floor } from "../entities/floor.entity.js";
import { Hotel } from "../entities/hotel.entity.js";
import { BedRoom, EmptyRoom, LobbyRoom, Room, ServiceRoom } from "../entities/room.entity.js";
import { BedRoomLevel } from "../enums/bedroom-level.enum.js";
import { HotelServiceMock } from "./hotel.service.mock.js";

const test = anyTest as TestInterface<{
  db: DatabaseServiceMock,
  service: BuildService,
}>;

test.before(async (t) => {
  const databaseServiceMock = new DatabaseServiceMock();
  await databaseServiceMock.create();
  t.context.db = databaseServiceMock;

  const hotelServiceMock = new HotelServiceMock(databaseServiceMock);
  await hotelServiceMock.create();

  t.context.service = configureServiceTest({
    service: BuildService,
    mocks: [
      databaseServiceMock.mock,
      hotelServiceMock.mock
    ]
  });

  await t.context.service.init();
});

test.after.always(async (t) => {
  await t.context.db.destroy();
});

test.beforeEach(async (t) => {
  const entityManager = t.context.db.connection.createEntityManager();

  const room = (position: number, entityClass: EntityTarget<Room> = EmptyRoom) => entityManager.create(entityClass, { position });

  await entityManager.save(Hotel, [{
    id: 1,
    playerId: "5d9bac23-b2a7-4ea8-ba49-83d60728c642",
    name: "First",
    money: 99999,
    floors: [
      {
        level: 0,
        rooms: [room(0), room(1), room(2, LobbyRoom)]
      },
      {
        level: 1,
        rooms: [room(0), room(1, ServiceRoom), entityManager.create(BedRoom, { position: 2, buildingEndAt: new Date(Date.now() + 100000) })]
      },
      {
        level: 2,
        rooms: [
          entityManager.create(BedRoom, { position: 0, level: BedRoomLevel.ONE }),
          entityManager.create(BedRoom, { position: 1, level: BedRoomLevel.TWO }),
          entityManager.create(BedRoom, { position: 2, level: BedRoomLevel.THREE })
        ]
      }
    ]
  }]);
});

test.afterEach.always(async (t) => {
  await t.context.db.connection.createQueryBuilder().delete().from(Room).execute();
  await t.context.db.connection.createQueryBuilder().delete().from(Floor).execute();
  await t.context.db.connection.createQueryBuilder().delete().from(Hotel).execute();
});

test.serial("BuildService.buildNewBedRoom invalid floor level (must be > 0)", async (t) => {
  const room = await t.context.db.connection.createQueryBuilder(Room, "r")
    .leftJoin("floor", "f", "f.id = r.floorId")
    .where("f.hotelId = :hotelId AND f.level = :level AND r.position = :position", { hotelId: 1, level: 0, position: 0 })
    .getOne();

  const result = await t.context.service.buildNewBedRoom(1, room!);

  t.false(result.succeeded, "must not succeed");
  t.is(result.status, ServiceResultStatus.invalid, "operation must be invalid");
});

test.serial("BuildService.buildNewBedRoom invalid hotel not enough money", async (t) => {
  await t.context.db.connection.createQueryBuilder(Hotel, "h")
    .update({ money: 0 })
    .execute();

  const room = await t.context.db.connection.createQueryBuilder(Room, "r")
    .leftJoin("floor", "f", "f.id = r.floorId")
    .where("f.hotelId = :hotelId AND f.level = :level AND r.position = :position", { hotelId: 1, level: 1, position: 0 })
    .getOne();

  const result = await t.context.service.buildNewBedRoom(1, room!);

  t.false(result.succeeded, "must not succeed");
  t.is(result.status, ServiceResultStatus.forbidden, "operation must be forbidden");
});

test.serial("BuildService.buildNewBedRoom valid", async (t) => {
  const room = await t.context.db.connection.createQueryBuilder(Room, "r")
    .leftJoin("floor", "f", "f.id = r.floorId")
    .where("f.hotelId = :hotelId AND f.level = :level AND r.position = :position", { hotelId: 1, level: 1, position: 0 })
    .getOne();

  const result = await t.context.service.buildNewBedRoom(1, room!);

  t.true(result.succeeded, "must succeed");
  t.is(result.value!.id, room!.id, "must be same id");
  t.is(result.value!.level.level, 1, "bedroom level must be set to 1");
  t.true(result.value!.buildingEndAt! > new Date(), "building end date must be future");
});

test.serial("BuildService.buildNewBedRoom valid (room is not empty)", async (t) => {
  const room = await t.context.db.connection.createQueryBuilder(Room, "r")
    .leftJoin("floor", "f", "f.id = r.floorId")
    .where("f.hotelId = :hotelId AND f.level = :level AND r.position = :position", { hotelId: 1, level: 1, position: 1 })
    .getOne();

  const result = await t.context.service.buildNewBedRoom(1, room!);

  t.true(result.succeeded, "must succeed");
  t.is(result.value!.id, room!.id, "must be same id");
  t.is(result.value!.level.level, 1, "bedroom level must be set to 1");
  t.true(result.value!.buildingEndAt! > new Date(), "building end date must be future");
});

test.serial("BuildService.upgradeBedRoom invalid room not built", async (t) => {
  const bedRoom = await t.context.db.connection.createQueryBuilder(BedRoom, "r")
    .leftJoin("floor", "f", "f.id = r.floorId")
    .where("f.hotelId = :hotelId AND f.level = :level AND r.position = :position", { hotelId: 1, level: 1, position: 2 })
    .getOne();

  const result = await t.context.service.upgradeBedRoom(1, bedRoom!);

  t.false(result.succeeded, "must not succeed");
  t.is(result.status, ServiceResultStatus.invalid, "operation must be invalid");
});

test.serial("BuildService.upgradeBedRoom invalid room type", async (t) => {
  const bedRoom = await t.context.db.connection.createQueryBuilder(BedRoom, "r")
    .leftJoin("floor", "f", "f.id = r.floorId")
    .where("f.hotelId = :hotelId AND f.level = :level AND r.position = :position", { hotelId: 1, level: 1, position: 0 })
    .getOne();

  const result = await t.context.service.upgradeBedRoom(1, bedRoom!);

  t.false(result.succeeded, "must not succeed");
  t.is(result.status, ServiceResultStatus.invalid, "operation must be invalid");
});

test.serial("BuildService.upgradeBedRoom invalid room already at max level", async (t) => {
  const bedRoom = await t.context.db.connection.createQueryBuilder(BedRoom, "r")
    .leftJoin("floor", "f", "f.id = r.floorId")
    .where("f.hotelId = :hotelId AND f.level = :level AND r.position = :position", { hotelId: 1, level: 2, position: 2 })
    .getOne();

  const result = await t.context.service.upgradeBedRoom(1, bedRoom!);

  t.false(result.succeeded, "must not succeed");
  t.is(result.status, ServiceResultStatus.invalid, "operation must be invalid");
});

test.serial("BuildService.upgradeBedRoom invalid hotel not enough money", async (t) => {
  await t.context.db.connection.createQueryBuilder(Hotel, "h")
    .update({ money: 0 })
    .execute();

  const bedRoom = await t.context.db.connection.createQueryBuilder(BedRoom, "r")
    .leftJoin("floor", "f", "f.id = r.floorId")
    .where("f.hotelId = :hotelId AND f.level = :level AND r.position = :position", { hotelId: 1, level: 2, position: 0 })
    .getOne();

  const result = await t.context.service.upgradeBedRoom(1, bedRoom!);

  t.false(result.succeeded, "must not succeed");
  t.is(result.status, ServiceResultStatus.forbidden, "operation must be forbidden");
});

test.serial("BuildService.upgradeBedRoom valid", async (t) => {
  const bedRoom = await t.context.db.connection.createQueryBuilder(BedRoom, "r")
    .leftJoin("floor", "f", "f.id = r.floorId")
    .where("f.hotelId = :hotelId AND f.level = :level AND r.position = :position", { hotelId: 1, level: 2, position: 0 })
    .getOne();

  const result = await t.context.service.upgradeBedRoom(1, bedRoom!);

  t.true(result.succeeded, "must succeed");
  t.is(result.value!.level.level, 2, "bedroom level must increase");
  t.true(result.value!.buildingEndAt! > new Date(), "building end date must be future");
});
