import anyTest, { TestInterface } from "ava";
import { configureServiceTest } from "fastify-decorators/testing";
import { EntityTarget } from "typeorm";

import { ServiceResultStatus } from "../../../services/service.js";
import { DatabaseServiceMock } from "../../../tests/database.service.mock.js";
import { BuildService } from "../build.service.js";
import { Floor } from "../entities/floor.entity.js";
import { Hotel } from "../entities/hotel.entity.js";
import { EmptyRoom, LobbyRoom, Room } from "../entities/room.entity.js";
import { HotelServiceMock } from "./hotel.service.mock.js";

const test = anyTest as TestInterface<{
  db: DatabaseServiceMock,
  service: BuildService,
}>;

test.before(async (t) => {
  const databaseServiceMock = new DatabaseServiceMock();
  await databaseServiceMock.create();
  t.context.db = databaseServiceMock;

  const hotelServiceMock = new HotelServiceMock(databaseServiceMock);
  await hotelServiceMock.create();

  t.context.service = configureServiceTest({
    service: BuildService,
    mocks: [
      databaseServiceMock.mock,
      hotelServiceMock.mock
    ]
  });

  await t.context.service.init();
});

test.after.always(async (t) => {
  await t.context.db.destroy();
});

test.beforeEach(async (t) => {
  const entityManager = t.context.db.connection.createEntityManager();

  const room = (position: number, entityClass: EntityTarget<Room> = EmptyRoom) => entityManager.create(entityClass, { position });

  await entityManager.save(Hotel, [{
    id: 1,
    playerId: "5d9bac23-b2a7-4ea8-ba49-83d60728c642",
    name: "First",
    floors: [
      {
        level: 0,
        rooms: [room(0), room(1), room(2, LobbyRoom)]
      }
    ]
  }]);
});

test.afterEach.always(async (t) => {
  await t.context.db.connection.createQueryBuilder().delete().from(Room).execute();
  await t.context.db.connection.createQueryBuilder().delete().from(Floor).execute();
  await t.context.db.connection.createQueryBuilder().delete().from(Hotel).execute();
});

test.serial("BuildService.buildNewExtension invalid floorLevel", async (t) => {
  const roomsCountBefore = await t.context.service.getRoomsCount(1);

  const result = await t.context.service.buildNewExtension(1, { floorLevel: 2, position: 2 });

  const roomsCountAfter = await t.context.service.getRoomsCount(1);

  t.false(result.succeeded, "must not succeed");
  t.is(result.status, ServiceResultStatus.invalid, "operation must be invalid");
  t.is(roomsCountAfter, roomsCountBefore, "no new room must be created");
});

test.serial("BuildService.buildNewExtension invalid left position", async (t) => {
  const roomsCountBefore = await t.context.service.getRoomsCount(1);

  const result = await t.context.service.buildNewExtension(1, { floorLevel: 0, position: -2 });

  const roomsCountAfter = await t.context.service.getRoomsCount(1);

  t.false(result.succeeded, "must not succeed");
  t.is(result.status, ServiceResultStatus.invalid, "operation must be invalid");
  t.is(roomsCountAfter, roomsCountBefore, "no new room must be created");
});

test.serial("BuildService.buildNewExtension invalid right position", async (t) => {
  const roomsCountBefore = await t.context.service.getRoomsCount(1);

  const result = await t.context.service.buildNewExtension(1, { floorLevel: 0, position: 4 });

  const roomsCountAfter = await t.context.service.getRoomsCount(1);

  t.false(result.succeeded, "must not succeed");
  t.is(result.status, ServiceResultStatus.invalid, "operation must be invalid");
  t.is(roomsCountAfter, roomsCountBefore, "no new room must be created");
});

test.serial("BuildService.buildNewExtension invalid conflict position", async (t) => {
  const roomsCountBefore = await t.context.service.getRoomsCount(1);

  const result = await t.context.service.buildNewExtension(1, { floorLevel: 0, position: 1 });

  const roomsCountAfter = await t.context.service.getRoomsCount(1);

  t.false(result.succeeded, "must not succeed");
  t.is(result.status, ServiceResultStatus.invalid, "operation must be invalid");
  t.is(roomsCountAfter, roomsCountBefore, "no new room must be created");
});

test.serial("BuildService.buildNewExtension valid left position", async (t) => {
  const roomsCountBefore = await t.context.service.getRoomsCount(1);

  const result = await t.context.service.buildNewExtension(1, { floorLevel: 0, position: -1 });

  const roomsCountAfter = await t.context.service.getRoomsCount(1);

  t.true(result.succeeded, "must succeed");
  t.is(roomsCountAfter, roomsCountBefore + 1, "must build a new room");
  t.is(result.value!.position, 0, "position must be correct");
});

test.serial("BuildService.buildNewExtension valid right position (lobby must stay on right)", async (t) => {
  const roomsCountBefore = await t.context.service.getRoomsCount(1);

  const result = await t.context.service.buildNewExtension(1, { floorLevel: 0, position: 3 });

  const roomsCountAfter = await t.context.service.getRoomsCount(1);

  t.true(result.succeeded, "must succeed");
  t.is(roomsCountAfter, roomsCountBefore + 1, "must build a new room");
  t.is(result.value!.position, 2, "position must be correct");

  const floor = await t.context.db.connection.getRepository(Floor).findOne({
    hotelId: 1,
    level: 0
  }, {
    relations: ["rooms"]
  });

  const rightRoom = floor!.rooms!.reduce((prev, curr) => curr.position > prev.position ? curr : prev);
  t.true(rightRoom instanceof LobbyRoom, "the last room must be lobby");
  t.is(rightRoom.position, 3, "lobby must be the last room");
});

test.serial("BuildService.buildNewExtension valid top position", async (t) => {
  const roomsCountBefore = await t.context.service.getRoomsCount(1);

  const result = await t.context.service.buildNewExtension(1, { floorLevel: 1, position: 1 });

  const roomsCountAfter = await t.context.service.getRoomsCount(1);

  t.true(result.succeeded, "must succeed");
  t.is(roomsCountAfter, roomsCountBefore + 1, "must build a new room");
  t.is(result.value!.position, 1, "position must be correct");

  const floor = await t.context.db.connection.getRepository(Floor).findOne({
    hotelId: 1,
    level: 1
  }, {
    relations: ["rooms"]
  });

  t.is(floor!.level, 1, "must build a floor");
  t.true(floor!.rooms?.some(r => r.id === result.value!.id), "new room must be on new floor");
});
