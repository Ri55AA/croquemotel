import anyTest, { TestInterface } from "ava";
import { configureServiceTest } from "fastify-decorators/testing";

import { ServiceResultStatus } from "../../../services/service.js";
import { DatabaseServiceMock } from "../../../tests/database.service.mock.js";
import { Hotel } from "../entities/hotel.entity.js";
import { LobbyRoom } from "../entities/room.entity.js";
import { HotelService } from "../hotel.service.js";

const test = anyTest as TestInterface<{
  db: DatabaseServiceMock,
  service: HotelService,
}>;

test.before(async (t) => {
  t.context.db = new DatabaseServiceMock();
  await t.context.db.create();

  const entityManager = t.context.db.connection.createEntityManager();

  await entityManager.save(Hotel, [{
    id: 1,
    playerId: "209288fd-d97d-42ac-bf42-3bc02d7d085c",
    name: "First",
    money: 1000,
    floors: [{
      level: 0,
      rooms: [new LobbyRoom()]
    }]
  }, {
    id: 2,
    playerId: "eb832790-961a-48f3-99da-1fa090754a4e",
    name: "Second",
    money: 1000,
    floors: [{
      level: 0,
      rooms: [new LobbyRoom()]
    }]
  }]);

  t.context.service = configureServiceTest({
    service: HotelService,
    mocks: [
      t.context.db.mock
    ]
  });

  await t.context.service.init();
});

test.after.always(async (t) => {
  await t.context.db.destroy();
});

test.serial("HotelService.canPay successful", async (t) => {
  const result = await t.context.service.canPay(1, 500);

  t.true(result.succeeded, "must succeed");
  t.true(result.value);
});

test.serial("HotelService.canPay erroneous", async (t) => {
  const result = await t.context.service.canPay(1, 1500);

  t.true(result.succeeded, "must succeed");
  t.false(result.value);
});

test.serial("HotelService.pay successful (with update)", async (t) => {
  const result = await t.context.service.pay(1, 500);

  t.true(result.succeeded, "must succeed");
  t.is(result.value!.money, 500);

  const updatedResult = await t.context.db.connection.getRepository(Hotel).findOne(1);
  t.is(updatedResult!.money, 500);
});

test.serial("HotelService.pay erroneous (with update)", async (t) => {
  const result = await t.context.service.pay(2, 1500);

  t.false(result.succeeded, "must not succeed");
  t.is(result.status, ServiceResultStatus.forbidden);

  const updatedResult = await t.context.db.connection.getRepository(Hotel).findOne(2);
  t.is(updatedResult!.money, 1000);
});
