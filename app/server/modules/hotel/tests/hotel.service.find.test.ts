import anyTest, { TestInterface } from "ava";
import { configureServiceTest } from "fastify-decorators/testing";

import { ServiceResultStatus } from "../../../services/service.js";
import { DatabaseServiceMock } from "../../../tests/database.service.mock.js";
import { Hotel } from "../entities/hotel.entity.js";
import { LobbyRoom } from "../entities/room.entity.js";
import { HotelService } from "../hotel.service.js";

const test = anyTest as TestInterface<{
  db: DatabaseServiceMock,
  service: HotelService,
}>;

test.before(async (t) => {
  t.context.db = new DatabaseServiceMock();
  await t.context.db.create();

  const entityManager = t.context.db.connection.createEntityManager();

  await entityManager.save(Hotel, [{
    id: 1,
    playerId: "209288fd-d97d-42ac-bf42-3bc02d7d085c",
    name: "First",
    floors: [{
      level: 0,
      rooms: [new LobbyRoom()]
    }]
  }, {
    id: 2,
    playerId: "eb832790-961a-48f3-99da-1fa090754a4e",
    name: "Second",
    floors: [{
      level: 0,
      rooms: [new LobbyRoom()]
    }]
  }]);

  t.context.service = configureServiceTest({
    service: HotelService,
    mocks: [
      t.context.db.mock
    ]
  });

  await t.context.service.init();
});

test.after.always(async (t) => {
  await t.context.db.destroy();
});

test.serial("HotelService.findHotel with valid hotelId", async (t) => {
  const result = await t.context.service.findHotel(1);

  t.true(result.succeeded, "must succeed");
  t.is(result.value!.id, 1, "must have a valid value");
});

test.serial("HotelService.findHotel with valid playerId", async (t) => {
  const result = await t.context.service.findHotel("209288fd-d97d-42ac-bf42-3bc02d7d085c");

  t.true(result.succeeded, "must succeed");
  t.is(result.value!.id, 1, "must have a valid value");
});

test.serial("HotelService.findHotel with invalid hotelId", async (t) => {
  const result = await t.context.service.findHotel(42);

  t.false(result.succeeded, "must not succeed");
  t.is(result.status, ServiceResultStatus.notFound, "must be not found");
});

test.serial("HotelService.findOrCreateHotel with non existing player", async (t) => {
  const result = await t.context.service.findOrCreateHotel("5d9bac23-b2a7-4ea8-ba49-83d60728c642");

  t.true(result.succeeded, "must succeed");
  t.is(result.value!.id, 3, "must have a valid value");
});

test.serial("HotelService.findOrCreateHotel with existing player", async (t) => {
  const result = await t.context.service.findOrCreateHotel("209288fd-d97d-42ac-bf42-3bc02d7d085c");

  t.true(result.succeeded, "must succeed");
  t.is(result.value!.id, 1, "must have a valid value");
});
