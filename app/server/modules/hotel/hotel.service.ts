import { Initializer, Service } from "fastify-decorators";
import { Repository } from "typeorm";

import { DatabaseService } from "../../services/database.service.js";
import { BaseService, ServiceResult } from "../../services/service.js";
import { Hotel } from "./entities/hotel.entity.js";
import { LobbyRoom } from "./entities/room.entity.js";

@Service()
export class HotelService extends BaseService {
  private hotelRepository!: Repository<Hotel>;

  constructor(
    private db: DatabaseService
  ) {
    super();
  }

  @Initializer([DatabaseService])
  init() {
    this.hotelRepository = this.db.connection.getRepository(Hotel);
  }

  /**
   * Gets a hotel from DB by its ID or player (owner) ID.
   * @param hotelIdOrPlayerId ID of the hotel to find, or associated player (owner) ID.
   * @param relations Relations of entity to load.
   * @returns A successful result with the hotel entity or a erroneous result if the hotel is not found.
   */
  public async findHotel(hotelIdOrPlayerId: number | string, relations?: string[]): Promise<ServiceResult<Hotel>> {
    const where = typeof hotelIdOrPlayerId === "number"
      ? { id: hotelIdOrPlayerId }
      : { playerId: hotelIdOrPlayerId };

    const hotel = await this.hotelRepository.findOne({
      where,
      relations
    });

    return hotel
      ? this.succeed(hotel)
      : this.failNotFound();
  }

  /**
   * Gets the hotel associated to the given player, or create a new one if none.
   * @param playerId Hotel's player (owner) ID.
   * @returns A successful result with the hotel entity.
   */
  public async findOrCreateHotel(playerId: string): Promise<ServiceResult<Hotel>> {
    const hotelResult = await this.findHotel(playerId);

    if (hotelResult.succeeded) {
      return hotelResult;
    }

    const hotel = await this.hotelRepository.save({
      playerId,
      name: "Random name",
      floors: [
        {
          level: 0,
          rooms: [
            new LobbyRoom()
          ]
        }
      ]
    });

    return this.succeed(hotel);
  }

  /**
   * Checks if the player's hotel can pay the given amount.
   * @param hotelOrPlayer The hotel to check.
   * @param amount The amount to pay.
   * @returns A successful result with a boolean telling if the hotel can pay, an erroneous result if the hotel is not found.
   */
  public async canPay(hotelOrPlayer: Hotel | number | string, amount: number): Promise<ServiceResult<boolean>> {
    const hotel = await this.get(hotelOrPlayer);

    if (!hotel) {
      return this.failNotFound();
    }

    return this.succeed(hotel.money >= amount);
  }

  /**
   * Makes the player's hotel pay the given amount.
   * @param hotelOrPlayer The hotel who pay.
   * @param amount The amount to pay.
   * @param update Tell if the entity should be updated.
   * @returns A successful result with the hotel entity, an erroneous result if the hotel is not found or cannot pay.
   */
  public async pay(hotelOrPlayer: Hotel | number | string, amount: number, update: boolean = true): Promise<ServiceResult<Hotel>> {
    const hotel = await this.get(hotelOrPlayer);
    if (!hotel) {
      return this.failNotFound();
    }

    const canPayResult = await this.canPay(hotel, amount);
    if (!canPayResult.value) {
      return this.failForbidden();
    }

    const newMoney = hotel.money - amount;

    if (update) {
      try {
        await this.hotelRepository.update(hotel.id, {
          money: newMoney
        });
      } catch (e) {
        return this.fail(e.message);
      }
    }

    hotel.money = newMoney;

    return this.succeed(hotel);
  }

  /**
   * Adds fame to the hotel.
   * @param hotelOrPlayer The target hotel.
   * @param amount The amount of fame to add.
   * @param update Tell if the entity should be updated.
   * @returns A successful result with the hotel entity, an erroneous result if the hotel is not found or an error occurred.
   */
  public async addFame(hotelOrPlayer: Hotel | number | string, amount: number, update: boolean = true): Promise<ServiceResult<Hotel>> {
    const hotel = await this.get(hotelOrPlayer);
    if (!hotel) {
      return this.failNotFound();
    }

    const newFame = hotel.fame + amount;

    if (update) {
      try {
        await this.hotelRepository.update(hotel.id, {
          fame: newFame
        });
      } catch (e) {
        return this.fail(e.message);
      }
    }

    hotel.fame = newFame;

    return this.succeed(hotel);
  }

  /**
   * Returns the given hotel or a hotel from DB.
   * @param hotelOrPlayer Already fetch hotel, hotel ID or player ID.
   * @returns The given hotel or a hotel from DB if ID matches.
   */
  public async get(hotelOrPlayer: Hotel | number | string): Promise<Hotel | undefined> {
    if (typeof hotelOrPlayer === "object") {
      return hotelOrPlayer;
    }

    const hotelResult = await this.findHotel(hotelOrPlayer);

    if (!hotelResult.succeeded) {
      return undefined;
    }

    return hotelResult.value!;
  }
}
