import { Exclude } from "class-transformer";
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";

import { Hotel } from "./hotel.entity.js";
import { Room } from "./room.entity.js";

@Entity()
export class Staff {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column()
  hotelId!: number;

  @ManyToOne("Hotel")
  @JoinColumn({ name: "hotelId" })
  @Exclude()
  hotel?: Hotel;

  @ManyToOne("Room", "staffs", { nullable: true })
  @Exclude()
  room?: Room;

  @CreateDateColumn()
  @Exclude()
  createdAt!: Date;

  @UpdateDateColumn()
  @Exclude()
  updatedAt!: Date;
}
