import { Exclude } from "class-transformer";
import {
  ChildEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  TableInheritance,
  UpdateDateColumn
} from "typeorm";

import { Guest } from "../../guest/entities/guest.entity.js";
import { BedRoomLevel, BedRoomLevelTransformer } from "../enums/bedroom-level.enum.js";
import { RoomEquipment, RoomEquipmentTransformer } from "../enums/room-equipment.enum.js";
import { ServiceRoomType, ServiceRoomTypeTransformer } from "../enums/serviceroom-type.enum.js";
import { SpecialRoomType, SpecialRoomTypeTransformer } from "../enums/specialroom-type.enum.js";
import { Floor } from "./floor.entity.js";
import { Staff } from "./staff.entity.js";

type RoomConstructorParams = {floor?: Floor, position: number, buildingEndAt?: Date};

@Entity()
@TableInheritance({ column: { type: "varchar", name: "type" } })
export abstract class Room {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column()
  type!: string;

  @Column()
  floorId!: number;

  @ManyToOne("Floor", "rooms")
  @JoinColumn({ name: "floorId" })
  @Exclude()
  floor?: Floor;

  @Column({ default: 0 })
  position!: number;

  @OneToMany("Staff", "room")
  staffs?: Staff[];

  @Column({ nullable: true })
  buildingEndAt?: Date;

  @CreateDateColumn()
  @Exclude()
  createdAt!: Date;

  @UpdateDateColumn()
  @Exclude()
  updatedAt!: Date;

  constructor(params?: RoomConstructorParams) {
    if (!params) {
      return;
    }

    if (params.floor) {
      this.floor = params.floor;
    }
    this.position = params.position;
    this.buildingEndAt = params.buildingEndAt;
  }

  get isBuilt(): boolean {
    return typeof this.buildingEndAt === "undefined" || this.buildingEndAt < new Date();
  }
}

@ChildEntity()
export class EmptyRoom extends Room {

}

@ChildEntity()
export class BedRoom extends Room {
  @Column({ type: "int", default: 1, transformer: new BedRoomLevelTransformer() })
  level!: BedRoomLevel;

  @Column({ default: 0 })
  damages!: number;

  @OneToOne("Guest", { nullable: true })
  @JoinColumn()
  guest?: Guest;

  @Column({ type: "varchar", nullable: true, transformer: new RoomEquipmentTransformer() })
  equipment1?: RoomEquipment;

  @Column({ type: "varchar", nullable: true, transformer: new RoomEquipmentTransformer() })
  equipment2?: RoomEquipment;

  @Column({ nullable: true })
  serviceEndAt?: Date;
}

@ChildEntity()
export class SpecialRoom extends Room {
  @Column({ type: "varchar", transformer: new SpecialRoomTypeTransformer() })
  service!: SpecialRoomType;

  @Column({ nullable: true })
  serviceEndAt?: Date;

  @OneToOne("Guest", { nullable: true })
  @JoinColumn()
  // @Type(() => Guest)
  guest?: Guest;
}

@ChildEntity()
export class ServiceRoom extends Room {
  @Column({ type: "varchar", transformer: new ServiceRoomTypeTransformer() })
  service!: ServiceRoomType;

  @Column({ nullable: true })
  serviceEndAt?: Date;

  constructor(params?: RoomConstructorParams & { service: ServiceRoomType }) {
    super(params);

    if (!params) {
      return;
    }

    this.service = params.service;
  }
}

@ChildEntity()
export class LobbyRoom extends Room {

}

@ChildEntity()
export class LabRoom extends Room {
  @OneToOne("Guest", { nullable: true })
  @JoinColumn()
  guest?: Guest;

  @Column({ nullable: true })
  serviceEndAt?: Date;
}
