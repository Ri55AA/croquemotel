import { Exclude } from "class-transformer";
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";

import { Hotel } from "./hotel.entity.js";
import { Room } from "./room.entity.js";

@Entity()
export class Floor {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column()
  hotelId!: number;

  @ManyToOne("Hotel")
  @JoinColumn({ name: "hotelId" })
  @Exclude()
  hotel?: Hotel;

  @OneToMany("Room", "floor", { cascade: true })
  rooms?: Room[];

  @Column()
  level!: number;

  @Column({ default: 0 })
  paint!: number;

  @Column({ default: 0 })
  wallpaper!: number;

  @Column({ default: 0 })
  wallpaperTilt!: number;

  @Column({ nullable: true })
  lowerWall?: number;

  @CreateDateColumn()
  @Exclude()
  createdAt!: Date;

  @UpdateDateColumn()
  @Exclude()
  updatedAt!: Date;
}
