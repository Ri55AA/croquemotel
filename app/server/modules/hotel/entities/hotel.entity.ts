import { Expose } from "class-transformer";
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";

import { Guest } from "../../guest/entities/guest.entity.js";
import { NeonColor } from "../enums/neon-color.enum.js";
import { Floor } from "./floor.entity.js";
import { Staff } from "./staff.entity.js";

@Entity()
export class Hotel {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  playerId!: string;

  @OneToMany("Floor", "hotel", { cascade: true })
  floors?: Floor[];

  @Column()
  name?: string;

  @Column({ default: 0 })
  stars!: number;

  @Column({ default: 0 })
  @Expose({ groups: ["player"] })
  money!: number;

  @Column({ default: 0 })
  fame!: number;

  @Column({ default: 0 })
  @Expose({ groups: ["player"] })
  level!: number;

  @Column({ enum: NeonColor, default: NeonColor.red })
  neonColour!: NeonColor;

  @OneToMany("Staff", "hotel", { cascade: true })
  @Expose({ groups: ["player"] })
  staffs?: Staff[];

  @OneToMany("Guest", "hotel", { cascade: true })
  waitingList?: Guest[];

  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;
}
