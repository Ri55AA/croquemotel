import { FastifyPluginCallback, FastifyReply, FastifyRequest } from "fastify";
import fp from "fastify-plugin";

import { Hotel } from "./entities/hotel.entity.js";

declare module "fastify" {
  interface FastifyRequest {
    getHotelId: () => number | undefined;
  }

  interface FastifyReply {
    setHotelId: (hotel: Hotel | number) => void;
  }
}

// eslint-disable-next-line require-await
const fastifyHotelPlugin: FastifyPluginCallback = fp(async (fastify) => {
  fastify.decorateRequest("getHotelId", function (this: FastifyRequest) {
    return this.session.get("hotelId");
  });

  fastify.decorateReply("setHotelId", function (this: FastifyReply, hotel: Hotel | number) {
    const hotelId = typeof hotel === "number" ? hotel : hotel.id;
    this.request.session.set("hotelId", hotelId);
  });
}, {
  fastify: "3.x",
  decorators: {
    request: ["session"]
  }
});

export default fastifyHotelPlugin;
