import { ValueTransformer } from "typeorm";

import { RoomBuildConfig } from "./room-build-config.interface";

export class BedRoomLevel {
  static ONE = new BedRoomLevel(1, {
    price: 100,
    delay: 5 * 60 * 1000,
    fame: 0,
    requiredHotelLevel: 0,
    disallowFloor0: true
  }, {
    price: 6,
    delay: 2 * 60 * 1000
  }, 0);

  static TWO = new BedRoomLevel(2, {
    price: 250,
    delay: 10 * 60 * 1000,
    fame: 1,
    requiredHotelLevel: 0,
    disallowFloor0: true
  }, {
    price: 9,
    delay: 2 * 60 * 1000
  }, 0.2);

  static THREE = new BedRoomLevel(3, {
    price: 600,
    delay: 10 * 60 * 1000,
    fame: 2,
    requiredHotelLevel: 0,
    disallowFloor0: true
  }, {
    price: 12,
    delay: 2 * 60 * 1000
  }, 0.6);

  private constructor(
    public level: number,
    public buildConfig: RoomBuildConfig,
    public repairConfig: {
      price: number,
      delay: number
    },
    public bonus: number
  ) { }

  static from(level: number): BedRoomLevel {
    return Object.values(BedRoomLevel).find(value => value instanceof BedRoomLevel && value.level === level);
  }
}

export class BedRoomLevelTransformer implements ValueTransformer {
  to(value: BedRoomLevel): any {
    return value?.level;
  }

  from(value: any): BedRoomLevel {
    return BedRoomLevel.from(value);
  }
}
