import { ValueTransformer } from "typeorm";

export class RoomEquipment {
  static BUFFET = new RoomEquipment("buffet");
  static HIFI_SYSTEM = new RoomEquipment("hifi_system");
  static HUMIDIFIER = new RoomEquipment("humidifier");
  static STINK_BOMB = new RoomEquipment("stink_bomb");
  static FRIEND = new RoomEquipment("friend");
  static RADIATOR = new RoomEquipment("radiator");
  static OLD_BUFFET = new RoomEquipment("old_buffet");
  static DJ = new RoomEquipment("dj");
  static LABY_CUPBOARD = new RoomEquipment("laby_cupboard");
  static MATTRESS = new RoomEquipment("mattress");
  static FIREWORKS = new RoomEquipment("fireworks");
  static WALLET = new RoomEquipment("wallet");
  static ISOLATION = new RoomEquipment("isolation");

  private constructor(
    public name: string
  ) { }

  static from(name: string): RoomEquipment {
    return Object.values(RoomEquipment).find(value => value instanceof RoomEquipment && value.name === name);
  }
}

export class RoomEquipmentTransformer implements ValueTransformer {
  to(value: RoomEquipment): any {
    return value?.name;
  }

  from(value: any): RoomEquipment {
    return RoomEquipment.from(value);
  }
}
