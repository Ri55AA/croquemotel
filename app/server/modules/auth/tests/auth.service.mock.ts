import { FastifyInstance } from "fastify";

import { AuthService } from "../auth.service.js";
import { InvalidAuthTokenError } from "../InvalidAuthTokenError.js";

export class AuthServiceMock {
  private service!: _AuthServiceMock;

  constructor() { }

  create(fastify: FastifyInstance) {
    this.service = new _AuthServiceMock(fastify);
  }

  get mock() {
    return {
      provide: AuthService,
      useValue: this.service as any
    };
  }

  get isAuthenticated(): boolean {
    return this.service.isAuthenticated;
  }

  set isAuthenticated(value: boolean) {
    this.service.isAuthenticated = value;
  }
}

class _AuthServiceMock {
  public isAuthenticated = true;

  constructor(private fastify: FastifyInstance) { }

  public authenticate() {
    return this.fastify.auth([
      (_, __, done) => done(this.isAuthenticated ? undefined : new InvalidAuthTokenError())
    ]);
  }
}
