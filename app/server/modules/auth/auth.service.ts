import { FastifyInstance, FastifyRequest, preHandlerHookHandler } from "fastify";
import { FastifyInstanceToken, Inject, Service } from "fastify-decorators";
import superagent from "superagent";

import { BaseService, ServiceResult } from "../../services/service.js";
import { AuthToken } from "./AuthToken.js";
import { InvalidAuthTokenError } from "./InvalidAuthTokenError.js";

@Service()
export class AuthService extends BaseService {
  @Inject(FastifyInstanceToken)
  private fastify!: FastifyInstance;

  /**
   * Fastify route pre-handler to require authentication.
   */
  public get authenticate(): preHandlerHookHandler {
    return this.fastify.auth([
      (request, _, done) => {
        const isAuthenticated = this.authenticateRequest(request);
        done(isAuthenticated ? undefined : new InvalidAuthTokenError());
      }
    ]);
  }

  /**
   * Generates an ETwin OAuth authorization URI.
   * @param redirectUri OAuth callback URI.
   * @param state Request state, to validate after callback reply.
   */
  public generateAuthorizationUri(redirectUri: string, state: string): string {
    const { ETWIN_PORT, ETWIN_OAUTH_URI, ETWIN_OAUTH_KEY } = process.env;

    const oAuthUri = process.dev ? `http://localhost:${ETWIN_PORT}` : ETWIN_OAUTH_URI;
    redirectUri = encodeURIComponent(redirectUri);

    return `${oAuthUri}/oauth/authorize?access_type=offline&response_type=code&client_id=${ETWIN_OAUTH_KEY}&redirect_uri=${redirectUri}&scope=&state=${state}`;
  }

  /**
   * Gets the ETwin access token after successful login.
   * @param code OAuth code given after client successful login.
   */
  public async getAccessToken(code: string): Promise<ServiceResult<AuthToken>> {
    const { ETWIN_OAUTH_URI, ETWIN_OAUTH_KEY, ETWIN_OAUTH_SECRET } = process.env;

    const credentials = Buffer.from(`${ETWIN_OAUTH_KEY}:${ETWIN_OAUTH_SECRET}`).toString("base64");
    const response = await superagent.post(`${ETWIN_OAUTH_URI}/oauth/token`)
      .set("Authorization", `Basic ${credentials}`)
      .set("Content-Type", "application/json")
      .send({
        code,
        grant_type: "authorization_code"
      });

    if (!response.ok) {
      return this.fail(response.text);
    }

    const authToken = {
      accessToken: response.body.access_token,
      refreshToken: response.body.refresh_token,
      expiresAt: response.body.expires_in + Math.floor(Date.now() / 1000),
      tokenType: response.body.token_type
    };

    return this.succeed(authToken);
  }

  private authenticateRequest(request: FastifyRequest): boolean {
    const authToken = request.getAuthToken();
    return !this.isTokenExpired(authToken);
  }

  private isTokenExpired(token?: AuthToken): boolean {
    return !token || token.expiresAt < Math.floor(Date.now() / 1000);
  }
}
