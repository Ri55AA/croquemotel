import { FastifyPluginCallback, FastifyReply, FastifyRequest } from "fastify";
import fp from "fastify-plugin";

import { AuthToken } from "./AuthToken.js";

declare module "fastify" {
  interface FastifyRequest {
    getAuthToken: () => AuthToken | undefined;
  }

  interface FastifyReply {
    setAuthToken: (token: AuthToken) => void;
    deleteSession: () => void;
  }
}

// eslint-disable-next-line require-await
const fastifyAuthPlugin: FastifyPluginCallback = fp(async (fastify) => {
  fastify.decorateRequest("getAuthToken", function (this: FastifyRequest) {
    return this.session.get("authToken");
  });

  fastify.decorateReply("setAuthToken", function (this: FastifyReply, token: AuthToken) {
    this.setCookie("auth", "1", { httpOnly: false, sameSite: "lax", secure: true, path: "/" });
    this.request.session.set("authToken", token);
  });

  fastify.decorateReply("deleteSession", function (this: FastifyReply) {
    this.clearCookie("auth");
    this.request.session.delete();
  });
}, {
  fastify: "3.x",
  decorators: {
    request: ["session"],
    reply: ["cookie"]
  }
});

export default fastifyAuthPlugin;
