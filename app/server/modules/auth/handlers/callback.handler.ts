import { FastifyInstance, FastifyReply, FastifyRequest } from "fastify";
import { Controller, FastifyInstanceToken, GET, getInstanceByToken } from "fastify-decorators";

import { EtwinService } from "../../../services/etwin.service.js";
import { HotelService } from "../../hotel/hotel.service.js";
import { AuthService } from "../auth.service.js";

@Controller("/auth/callback")
export default class CallbackHandler {
  private static fastify = getInstanceByToken<FastifyInstance>(FastifyInstanceToken);

  constructor(
    private authService: AuthService,
    private etwinService: EtwinService,
    private hotelService: HotelService
  ) { }

  @GET("", {
    onRequest: CallbackHandler.fastify.csrfProtection
  })
  public async get(request: FastifyRequest<{ Querystring: { code: string } }>, reply: FastifyReply) {
    const tokenResult = await this.authService.getAccessToken(request.query.code);

    if (!tokenResult.succeeded) {
      reply.sendServiceResult(tokenResult);
      return;
    }

    const auth = await this.etwinService.getAuthSelf(tokenResult.value!.accessToken);

    const hotelResult = await this.hotelService.findOrCreateHotel(auth.user.id);

    if (!hotelResult.succeeded) {
      reply.sendServiceResult(hotelResult);
      return;
    }

    reply.setAuthToken(tokenResult.value!);
    reply.setHotelId(hotelResult.value!);

    reply.redirect("/");
  }
}
