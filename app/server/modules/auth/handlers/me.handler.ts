import { FastifyReply, FastifyRequest } from "fastify";
import { Controller, ErrorHandler, GET } from "fastify-decorators";

import { EtwinService } from "../../../services/etwin.service.js";
import { authenticate } from "../auth.helper.js";
import { InvalidAuthTokenError } from "../InvalidAuthTokenError.js";

@Controller("/auth/me")
export default class AuthMeHandler {
  constructor(
    private etwinService: EtwinService
  ) { }

  @GET("", {
    preHandler: authenticate
  })
  public async get(request: FastifyRequest, reply: FastifyReply) {
    const accessToken = request.getAuthToken()?.accessToken;

    if (!accessToken) {
      throw new InvalidAuthTokenError();
    }

    const auth = await this.etwinService.getAuthSelf(accessToken);

    reply.code(200).send(auth.user);
  }

  @ErrorHandler(InvalidAuthTokenError)
  handleInvalidToken(_error: InvalidAuthTokenError, _request: FastifyRequest, reply: FastifyReply) {
    reply.status(401).send();
  }
}
