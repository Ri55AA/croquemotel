import { getInstanceByToken } from "fastify-decorators";

import { AuthService } from "./auth.service.js";

export const authenticate = getInstanceByToken<AuthService>(AuthService).authenticate;
