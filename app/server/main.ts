import Fastify, { FastifyRequest } from "fastify";
import { FastifyInstance } from "fastify/types/instance";
import fastifyAuth from "fastify-auth";
import fastifyCsrf from "fastify-csrf";
import { bootstrap } from "fastify-decorators";
import fastifyFormbody from "fastify-formbody";
import fastifySecureSession from "fastify-secure-session";
import fastifySensible from "fastify-sensible";
import { dirname, resolve } from "path";
import qs from "qs";
import { fileURLToPath } from "url";

import authPlugin from "./modules/auth/auth.plugin.js";
import hotelPlugin from "./modules/hotel/hotel.plugin.js";
import fastifyNuxt from "./plugins/nuxt.plugin.js";
import servicePlugin from "./plugins/service.plugin.js";

process.dev = process.env.NODE_ENV !== "production";
process.test = process.env.NODE_ENV === "test";

const devLogger = {
  level: "debug",
  prettyPrint: {
    levelFirst: true
  }
};
interface SetupOptions {
  client?: boolean
}

export async function setup(options?: SetupOptions): Promise<FastifyInstance> {
  options = options ?? { };
  options.client = options.client ?? true;

  try {
    const fastify = Fastify({
      pluginTimeout: 60000 * 3,
      logger: process.dev && !process.test && devLogger,
      querystringParser: str => qs.parse(str)
    });

    const _dirname = dirname(fileURLToPath(import.meta.url));

    await fastify.register(fastifySensible);

    // FIXME: `Error: "k" must be crypto_secretbox_KEYBYTES bytes long`when using `key`
    await fastify.register(fastifySecureSession, {
      // key: readFileSync(join(_dirname, "session-secret-key")),
      secret: "averylogphrasebiggerthanthirtytwochars",
      salt: "mq9hDxBVDbspDR6n",
      cookie: {
        httpOnly: true,
        path: "/",
        sameSite: "lax",
        secure: true
      }
    });

    await fastify.register(fastifyCsrf, {
      sessionPlugin: "fastify-secure-session",
      getToken: (request: FastifyRequest) =>
        request.headers["x-xsrf-token"] ||
        (request.body && (request.body as any)._xsrf) ||
        (request.query && (request.query as any).state)
    });

    await fastify.register(fastifyAuth);

    await fastify.register(fastifyFormbody);

    await fastify.register(authPlugin);

    await fastify.register(servicePlugin);

    await fastify.register(hotelPlugin);

    if (options.client) {
      await fastify.register(fastifyNuxt);
    }

    await fastify.register(bootstrap, {
      directory: resolve(_dirname),
      mask: /\.(handler|controller)\.[jt]s$/
    });

    if (options.client) {
      fastify.nuxt("*");
    }

    return fastify;
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
}

export async function listen(fastify: FastifyInstance) {
  try {
    process.bindAddress = process.env.HOST || "0.0.0.0";
    process.bindPort = process.env.PORT || 3000;

    await fastify.listen(process.bindPort, process.bindAddress);

    console.info(`Listening on internal port ${process.bindPort}, externally available at ${process.env.APP_URI}`);
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
}
