import { classToPlain } from "class-transformer";
import { FastifyError, FastifyPluginCallback, FastifyReply, FastifyRequest } from "fastify";
import fp from "fastify-plugin";

import { ServiceResult, ServiceResultStatus } from "../services/service.js";

type ServicePluginOptions = {
  groups?: string[];
  successCode?: number;
};

declare module "fastify" {
  interface FastifyReply {
    sendServiceResult: <T>(result: ServiceResult<T>, options?: ServicePluginOptions) => FastifyReply;
  }
}

// eslint-disable-next-line require-await
const fastifyServicePlugin: FastifyPluginCallback = fp(async (fastify) => {
  fastify.decorateReply("sendServiceResult", function <T> (this: FastifyReply, result: ServiceResult<T>, options?: ServicePluginOptions) {
    const response: any = {};

    if (result.status === ServiceResultStatus.success) {
      response.success = true;
      response.code = options?.successCode ?? result.status;
      response.payload = classToPlain(result.value!, { groups: options?.groups, exposeUnsetFields: false });
    } else {
      response.error = true;
      response.code = result.status;
      response.messages = result.messages;
    }

    this
      .code(response.code)
      .send(response);
  });

  fastify.setErrorHandler(function (error: FastifyError, _: FastifyRequest, reply: FastifyReply) {
    const response = {
      error: true,
      code: error.statusCode ?? (error.validation ? 400 : 500),
      messages: [error.message]
    };

    reply
      .code(response.code)
      .send(response);
  });
}, {
  fastify: "3.x"
});

export default fastifyServicePlugin;
