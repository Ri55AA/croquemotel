#!/bin/bash
set -e

echo Using ${ENV:=development} environment

cd /app

if [ $ENV = "production" ]; then
  yarn run build && \
  yarn run start
else
  yarn run dev:docker
fi
